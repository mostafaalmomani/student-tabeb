import { HttpClient } from '@angular/common/http';
import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { BaseService } from '../services/base.service';

@Pipe({
  name: 'loadImage'
})
export class LoadImagePipe implements PipeTransform {

  imageToShow: SafeUrl | null = null;
   //apiUrl = 'https://api.exams-tabeeb.org/api/v2/';
   apiUrl = 'http://localhost:80/api/v2/';
  constructor(private http: HttpClient, private sanitizer: DomSanitizer, private base: BaseService) {
  }

  transform(value: any, ...args: any[]): any {
    const headers = new Headers({ 'Content-Type': 'image/*' });


    return `${this.base.apiUrl}readImage/${value}`;
    // this.http.get(`${environment.apiUrl}readImage/14FgkBfnhheVI-DZsUxT3ite`, {responseType: 'blob' })
    // .pipe(
    //   switchMap(blob => {
    //     const mediaType = 'image/*';
    //     const image = new Blob([blob], { type: mediaType });
    //     return this.convertBlobToBase64(image);
    //   })
    // )
    // .subscribe((base64ImageUrl: string) => {
    //   console.log(base64ImageUrl);
    //   return this.sanitizer.bypassSecurityTrustResourceUrl(base64ImageUrl);
    // });
  }


  convertBlobToBase64(blob: Blob): any {
    return Observable.create(observer => {
      const reader = new FileReader();
      const binaryString = reader.readAsDataURL(blob);
      reader.onload = (event: any) => {
        observer.next(event.target.result);
        observer.complete();
      };

      reader.onerror = (event: any) => {
        console.log('File could not be read: ' + event.target.error.code);
        observer.next(event.target.error.code);
        observer.complete();
      };
    });
  }

}
