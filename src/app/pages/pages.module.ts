import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';
import { ExamsComponent } from './exams/exams.component';
import { MarksComponent } from './marks/marks.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SharedModule } from '../shared/shared.module';
import { HeaderComponent } from './header/header.component';
import { ExamComponent } from './exam/exam.component';
import { FinishPageComponent } from './finish-page/finish-page.component';
import { CountdownModule } from 'ngx-countdown';
import { LoadImagePipe } from './load-image.pipe';
import { PreventRefreshGuard } from '../services/prevent-refresh.guard';
import { CoursesComponent } from './courses/courses.component';

@NgModule({
  // tslint:disable-next-line:max-line-length
  declarations: [ExamsComponent, MarksComponent, DashboardComponent,
    HeaderComponent, ExamComponent, FinishPageComponent, LoadImagePipe, CoursesComponent],
  imports: [
    CommonModule,
    SharedModule,
    PagesRoutingModule,
    CountdownModule,
  ],
  providers: [PreventRefreshGuard]

})
export class PagesModule { }
