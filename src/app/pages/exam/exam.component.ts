import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import { Rules } from 'src/app/core/models';
import { ExamsService } from 'src/app/services/exams.service';
import { ConfirmDialogComponent } from 'src/app/shared/confirm-dialog/confirm-dialog.component';
import html2canvas from 'html2canvas';
import jsPDF from 'jspdf';
import 'jspdf-autotable';

@Component({
  selector: 'app-exam',
  templateUrl: './exam.component.html',
  // styles: ['@import "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css";'],
  styleUrls: ['./exam.component.css']
})
export class ExamComponent implements OnInit {

  @ViewChild('screen') screen: ElementRef;
  @ViewChild('canvas') canvas: ElementRef;
  @ViewChild('downloadLink') downloadLink: ElementRef;

  Questions: { num, label, answers: any, studentAnswer, _id: any, mark, image }[] = [];
  Answers = [];
  pages: any = [];
  numOfPages = 1;
  currentPage = 0;
  returnBack = true;
  showResult = false;
  isRequiredQusetions = false;
  showError = false;
  finishExam = false;
  answerBeforeyouGo = false;
  isLastPage = false;
  numOfQperPage;
  examId: any;
  questionObj: Array<any>;
  title: any;
  start: Date;
  end: Date;
  from: any;
  to: any;
  numberOfQusetion: any;
  answerdObj: any = [];
  timeLeft: number;
  maxGreade: any;
  isActive: boolean;
  public pageOfItems: Array<any>;
  rules: Rules;
  items = [];
  p = 1;
  notes = [];
  note = '';
  isRandomSequence: boolean;
  dataImage: HTMLCanvasElement;
  courseNumber: any;
  loaded = false;
  head: any;
  data: any = [];
  incorrectAnswersIsVisable = false;
  public semester: any;
  constructor(private route: ActivatedRoute, private examService: ExamsService,
              private router: Router, public dialog: MatDialog, private sanitizer: DomSanitizer) {
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.examId = params.examid;
      this.examService.getExamInfo(this.examId)
        .subscribe(examInfo => {
          this.examService.isActive(this.examId)
            .subscribe(arg => {
              this.isActive = arg.isActive;
            });
          console.log(examInfo);
          this.title = examInfo.courseName;
          this.semester = examInfo.semester;
          this.start = new Date(examInfo.start);
          this.end = new Date(examInfo.end);
          this.maxGreade = examInfo.maxGreade;
          this.from = this.start.getHours() + ':' + this.start.getMinutes();
          this.to = this.end.getHours() + ':' + this.end.getMinutes();
          this.setTimeConfig();
          this.rules = examInfo.rulesId;
          this.applyRules();
          this.examService.getQusetion(this.examId)
            .subscribe(arg => {
              this.questionObj = arg;
              this.numberOfQusetion = this.questionObj.length;
              this.items = this.Questions;
              // this.getQuetions();
              // this.renderPages();
              this.loaded = true;
            });
        });
    });
  }

  private setTimeConfig(): void {
    let diffHour = (this.start.getHours() - this.end.getHours());
    let diffMin = (this.start.getMinutes() - this.end.getMinutes());

    if (diffHour === 1 && diffMin > 0) {
      diffHour = 0;
      diffMin = 60 - diffMin;
    }
    const hour = diffHour < 0 ? (diffHour * -1) * 3600 : diffHour * 3600;
    const min = diffMin < 0 ? (diffMin * -1) * 60 : diffMin * 60;
    const current: Date = new Date();
    this.timeLeft = (hour + min);

    if (current.getTime() > this.start.getTime()) {
      const localTimeHour = (current).getHours() * 3600;
      const localTimeMin = (current).getMinutes() * 60;
      const localTime = localTimeHour + localTimeMin;

      let newStartHour = (current).getHours() - this.start.getHours();
      const newEndHour = this.end.getHours() - (current).getHours();
      let newStartMin = (current).getMinutes() - this.start.getMinutes();

      if (newStartHour === 0 && newStartMin > 0 && newEndHour === 0) {
        newStartHour = 0;
      } else if (newStartHour === 1 && newStartMin > 0) {
        newStartHour = 0;
        newStartMin = 60 - newStartMin;
      } else if (newEndHour > 1 && newStartMin > 0) {
        const ss = 60 - this.start.getMinutes();
        newStartMin = ss - newStartMin;
      }

      const newdiffHour = ((current).getHours() - this.end.getHours());
      const newdiffMin = ((current).getMinutes() - this.end.getMinutes());

      const newhour = newStartHour < 0 ? (newStartHour * -1) * 3600 : newStartHour * 3600;
      const newmin = newStartMin < 0 ? (newStartMin * -1) * 60 : newStartMin * 60;


      const examTime = (this.timeLeft - (newhour + newmin));
      this.timeLeft = examTime;
      // < 0 ? (examTime * -1) : examTime;

      if (this.end.getTime() <= current.getTime()) {
        this.timeLeft = 0;
        this.router.navigate(['/MainView/pages/dashboard/exams']);
      }
    }
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngAfterViewInit(): void {
    this.examService.getQusetion(this.examId)
      .subscribe(arg => {
        this.questionObj = arg;
        if (this.isRandomSequence) {
          this.questionObj = this.shuffle(this.questionObj);
          this.questionObj.map(q => {
            q.answers = this.shuffle(q.answers);
          });
        }
        this.numberOfQusetion = this.questionObj.length;
        this.getQuetions();
        this.renderPages();
        this.items = this.Questions;
      });
  }

  shuffle(array): Array<any> {
    let currentIndex = array.length;
    let randomIndex;
    while (currentIndex !== 0) {
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex--;
      [array[currentIndex], array[randomIndex]] = [
        array[randomIndex], array[currentIndex]];
    }
    return array;
  }

  formatAMPM(date): any {
    let hours = date.getHours();
    let minutes = date.getMinutes();
    const ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    const strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
  }

  onChangePage(pageOfItems: Array<any>): void {
    // update current page of items
    this.pageOfItems = pageOfItems;
  }
  onTimerFinished(e: Event): void {
    // tslint:disable-next-line:no-string-literal
    if (e['action'] === 'done') {
      this.saveAnswers();
    }
  }

  // actions in page functions
  prevQuestion(): any {
    this.currentPage--;
    this.isLastPage = false;
  }
  nextQuestion(): any {
    if (this.isRequiredQusetions && this.notAnswerd(this.currentPage)) {
      this.answerBeforeyouGo = true;
      this.showError = true;
      return;
    }
    this.answerBeforeyouGo = false;
    this.showError = false;
    scroll(0, 0);
    this.currentPage++;
    if (this.currentPage === this.Questions.length) {
      this.isLastPage = true;
    }
  }

  notAnswerd(index): any {
    return this.pages[index].Questions[0].studentAnswer === '';
  }

  submitData(): any {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '250px',
      height: '180px',
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.saveAnswers();
      }
    });
  }

  onItemChange(a, q): any {
    let currentObjcts = [];
    let i = 0;
    currentObjcts = this.answerdObj.map(e => e.qustion._id);
    if (currentObjcts.includes(q._id)) {
      this.answerdObj.forEach(element => {
        if (element.qustion._id === q._id) {
          this.answerdObj.splice(i, 1);
        }
        i++;
      });
      this.answerdObj.push({ answer: a, qustion: q });
    } else {
      this.answerdObj.push({ answer: a, qustion: q });
    }
  }
  // rednring funcitions
  getQuetions(): any {
    Object.keys(this.questionObj).map((q, index) => {
      this.Questions.push({
        num: index + 1, studentAnswer: '', label: this.questionObj[q].questionText,
        _id: this.questionObj[q]._id, mark: this.questionObj[q].point, image: this.questionObj[q].imageQ,
        answers: Object.keys(this.questionObj[q].answers).map(a => this.questionObj[q].answers[a].text)
      });
      this.Answers.push('');
    });
  }

  renderPages(): any {
    // create pages that we need and
    // assign questions to those pages
    // if (this.numOfPages > this.Questions.length) {
    //     this.numOfPages = this.Questions.length;
    // }
    // if (this.numOfPages <= 0) {
    //   this.numOfPages = 1;
    // }
    // this.numOfQperPage = Math.ceil(this.Questions.length / this.numOfPages);
    for (let i = 0; i < this.Questions.length; i++) {
      this.pages.push({
        Num: i + 1,
        Questions: this.Questions.slice(i, i + 1)
        // Questions: this.Questions.slice(this.numOfQperPage * i, (this.numOfQperPage * i) + this.numOfQperPage)
      });
    }
  }

  applyRules(): any {
    this.numOfPages = this.rules.numberOfPage;
    this.returnBack = this.rules.returnBack;
    this.showResult = this.rules.resultVisable;
    this.isRandomSequence = this.rules.isRandomSequence;
    this.isRequiredQusetions = this.rules.isRequiredQusetions;
    this.incorrectAnswersIsVisable = this.rules.incorrectAnswersIsVisable;
  }

  addNote(): any {
    this.notes.push(this.note);
  }

  goToNotes(): any {
    window.scrollTo(0, 10000);
  }
  // validation functions
  everythingIsAnswerd(): any {
    return this.pages.filter(page => page.Questions[0].studentAnswer === '').length === 0;
  }

  nextQuestionCheck(): any {
    let cPageAnswers = this.pages[this.currentPage].Questions.slice(0, (this.currentPage + 1) * this.numOfQperPage);
    cPageAnswers = cPageAnswers.map(d => d.studentAnswer);
    return cPageAnswers.includes('');
  }

  saveAnswers(): void {
    this.showError = false;
    this.finishExam = false;
    this.calculateAnswers();
  }

  downloadImage(): void {
    html2canvas(this.screen.nativeElement).then(canvas => {
      window.scrollTo(0, 0);
      this.canvas.nativeElement.src = canvas.toDataURL();
      const doc = new jsPDF('p', 'pt', 'a4');
      const imgWidth = 600;
      const imgHeight = canvas.height * imgWidth / canvas.width;
      const contentDataURL = canvas.toDataURL('image/png');
      const position = 0;
      doc.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight);
      const pdf = new File([doc.output('blob')], this.title + '.pdf', {
        type: 'application/pdf',
      });
      const formData = new FormData();
      formData.append('examId', this.examId);
      formData.append('courseNumber', this.courseNumber);
      formData.append('Answers', pdf);
      this.examService.saveStudentAnswers(formData)
        .subscribe(arg => {
        });
    });
  }

  saveStudenAnswer(): any {

    // this.perpareData();
    const doc = new jsPDF('p', 'pt', 'a4');
    doc.text(this.title + ' / NO: ' + sessionStorage.getItem('studentNumber'), 10, 10);

    (doc as any).autoTable({
      head: this.head,
      body: this.data,
      columnStyles: {
        3: { columnWidth: 500 }
      }
    });
    const pdf = new File([doc.output('blob')], this.title + '.pdf', {
      type: 'application/pdf',
    });
    // doc.save(this.title + '.pdf');

    const formData = new FormData();
    formData.append('examId', this.examId);
    formData.append('courseNumber', this.courseNumber);
    formData.append('Answers', pdf);
    this.examService.saveStudentAnswers(formData)
      .subscribe(arg => {
      });
  }

  // private perpareData(): any {
  //   this.head = [['Question', 'Student Answer', 'Correct answer']];
  //   let QN = 1;
  //   let index = 0;
  //   this.Questions.forEach(q => {
  //     let dataObj = [];
  //     dataObj.push('Q' + QN + ' ) ' + q.label);
  //     dataObj.push(q.studentAnswer + '                    ');
  //     dataObj.push(this.questionObj[index].correctAnswer[0].text);
  //     QN++;
  //     index++;
  //     this.data.push(dataObj);
  //     dataObj = [];
  //   });

  //   const doc = new jsPDF('p', 'pt', 'a4');
  //   doc.text(this.title + ' / NO: ' + sessionStorage.getItem('studentNumber'), 10, 10);

  //   (doc as any).autoTable({
  //     head: this.head,
  //     body: this.data,
  //     columnStyles: {
  //       3: { columnWidth: 500 }
  //     }
  //   });
  //   const pdf = new File([doc.output('blob')], this.title + '.pdf', {
  //     type: 'application/pdf',
  //   });
  //   // doc.save(this.title + '.pdf');

  //   const formData = new FormData();
  //   formData.append('examId', this.examId);
  //   formData.append('courseNumber', this.courseNumber);
  //   formData.append('Answers', pdf);
  //   this.examService.saveStudentAnswers(formData)
  //     .subscribe(arg => {
  //     });
  // }

  calculateAnswers(): void {
    const correctAnswers = this.questionObj.map(q => q.correctAnswer[0].text);
    const currentAnswers = this.Questions.map(q => q.studentAnswer);
    const questionsTexts = this.questionObj.map(q => q.questionText);
    const mark = this.questionObj.map(q => q.point);

    const summary = [];
    for (let index = 0; index < correctAnswers.length; index++) {
      const cuA = this.findCurrentAnswers(questionsTexts[index], currentAnswers);
      summary.push({
        questionText: questionsTexts[index],
        correctAnswer: correctAnswers[index],
        currentAnswer: cuA
      });
    }
    this.examService.setSummary(summary);
    this.calculate();
    this.populateStudentAnswers(summary);
  }

  private populateStudentAnswers(summary: any[]): any {
    this.head = [['Question', 'Student Answer', 'Correct answer']];
    let QN = 1;
    let index = 0;
    summary.forEach(q => {
      let dataObj = [];
      dataObj.push('Q' + QN + ' ) ' + q.questionText);
      dataObj.push(q.currentAnswer + '                    ');
      dataObj.push(q.correctAnswer);
      QN++;
      index++;
      this.data.push(dataObj);
      dataObj = [];
    });

    const doc = new jsPDF('p', 'pt', 'a4');
    doc.text(this.title + ' / NO: ' + sessionStorage.getItem('studentNumber'), 10, 10);

    (doc as any).autoTable({
      head: this.head,
      body: this.data,
      columnStyles: {
        3: { columnWidth: 500 }
      }
    });
    const pdf = new File([doc.output('blob')], this.title + '.pdf', {
      type: 'application/pdf',
    });
    // doc.save(this.title + '.pdf');

    const formData = new FormData();
    formData.append('examId', this.examId);
    formData.append('courseNumber', this.courseNumber);
    formData.append('Answers', pdf);
    this.examService.saveStudentAnswers(formData)
      .subscribe(res => {
        this.examService.saveAnswers(this.answerdObj, this.examId)
          .subscribe(arg => {
            alert('انتهى الامتحان');
            this.router.navigate(['/finish-page', { exam: this.examId, mark: arg._id }]);
          });
      });
  }

  findCurrentAnswers(questionsTexts: any, currentAnswers: any): any {
    let ans = '';
    const question = this.questionObj.filter(q => q.questionText === questionsTexts);
    const answersTexts = question[0].answers.map(a => a.text);
    ans = answersTexts.filter(a => currentAnswers.includes(a));
    return ans[0];
  }

  calculate(): any{
    let result = 0;
    let index = 0;
    const mark = this.questionObj.map(q => q.point);
    this.answerdObj.forEach(element => {
      const question = this.questionObj.filter(q => q.questionText === element.qustion.label);
      if (question[0].correctAnswer[0].text === element.answer) {
          result += mark[index];
      }
      index++;
    });
    this.examService.setResult(result);

  }

}

