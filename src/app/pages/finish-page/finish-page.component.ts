import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ExamsService } from 'src/app/services/exams.service';
import { ExamsComponent } from '../exams/exams.component';

@Component({
  selector: 'app-finish-page',
  templateUrl: './finish-page.component.html',
  styleUrls: ['./finish-page.component.css']
})
export class FinishPageComponent implements OnInit {

  mark: any;
  showResult: any;
  maxGreade: any;
  status: any;
  result: any;
  summary: any[];
  incorrectAnswersIsVisable: any;
  constructor(private router: ActivatedRoute, private examservices: ExamsService) {
    this.summary = this.examservices.getSummary();
   }

  ngOnInit(): void {
    this.mark = this.router.snapshot.paramMap.get('mark');
    this.examservices.getMark(this.mark)
      .subscribe(arg => {
        this.showResult = arg.rules.resultVisable;
        this.maxGreade = arg.maxGreade;
        this.incorrectAnswersIsVisable = arg.rules.incorrectAnswersIsVisable;
        this.result = this.examservices.getResult();
        const status = Number.parseFloat(this.maxGreade) / 2;
        if (Number.parseFloat(this.result) >= status){
            this.status = 'ناجح';
        } else {
          this.status = 'راسب';
        }
      });
  }

}
