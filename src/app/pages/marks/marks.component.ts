import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MarksService } from 'src/app/services/marks.service';
import { StudentMarksObj } from 'src/app/core/models';
import { timer, Subscriber, Observable } from 'rxjs';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-marks',
  templateUrl: './marks.component.html',
  styleUrls: ['./marks.component.scss']
})
export class MarksComponent implements OnInit , AfterViewInit{
  courseNumber = 1;
  displayedColumns: string[] = [
    'courseName', 'courseNumber', 'firstExamMark',
    'secondExamMark',  'otherExamMark',
    'sum', 'order', 'avg'
  ];
  displayedColumns2: string[] = [
    'courseNumberM', 'courseNameM',
    'grade', 'courseStatus'
  ];
  public dataSource;
  public dataSource2;
  public marksObj = [];
  public dataSourseObj: StudentMarksObj[] = [];
  assessmentObj: any;
  assessments = [];
  coursesMark = [];
  firstMark: any;
  secondMark: any;
  otherMark: any;
  sum: any;
  public status: any;
  source = timer(0, 4000);
  subscription: any;
  @ViewChild('assesmentPaginator') assesmentPaginator: MatPaginator;
  @ViewChild('coursePaginator') coursePaginator: MatPaginator;
  constructor(private marksService: MarksService) {
    this.dataSource = new MatTableDataSource(this.assessments);
    this.dataSource2 = new MatTableDataSource(this.coursesMark);
  }

  ngOnInit(): void {
     this.subscription = this.source.subscribe(val => {
      this.loadCourseMark();
      this.loadMarks();
     });
  }

  private loadCourseMark(): any {
    this.marksService.getCoursesMark()
      .subscribe(arg => {
        this.coursesMark = arg.Markcourses;
        this.dataSource2.data = this.coursesMark;
      });
  }

  private loadMarks(): any {
    this.marksService.getMarks()
      .subscribe(res => {
        this.assessments = res.Assessments;
        this.dataSource.data = this.assessments;
        this.assessmentObj = this.assessments;
      });
  }

  ngOnDestroy(): any {
    this.subscription.unsubscribe();
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.assesmentPaginator;
    this.dataSource2.paginator = this.coursePaginator;
  }
}


