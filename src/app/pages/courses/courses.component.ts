import { Component, OnInit } from '@angular/core';
import { CoursesService } from '../../services/courses.service';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.scss']
})
export class CoursesComponent implements OnInit {

  displayedColumns: string[] = [
    'courseNumber', 'courseName',
    'grade', 'courseStatus'
  ];
  dataSource = [];

  constructor(private coursesService: CoursesService) {
    this.getCourses();
  }

  ngOnInit(): void {
  }

  getCourses(): any{
    this.coursesService.getCoursesForStudent().subscribe(courses => {
      console.log(courses);
    });
  }

}
