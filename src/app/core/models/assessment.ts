export class Questions {
  courseName: string;
  course: object;
  firstExamMark: number;
  secondExamMark: number; // ---------
  otherExamMark: number;
  order: number;
  avg: number;
  studentsId: string;
}
