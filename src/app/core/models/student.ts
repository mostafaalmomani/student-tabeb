export class Student {
    _id?: string;
    email: string;
    password: string;
    studentName?: string;
    token?: string;
    studentNumber?: string;
    level?: string;
    phone?: string;
  }
