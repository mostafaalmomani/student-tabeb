export class Questions {
  questionText: string;
  answers: object;
  correctAnswer: string;
  point: number; // ---------
  examId: string;
  sequenceQ: boolean;
  imageQ: string;
  explanation: string;
  TimeSpent: Date;
}
