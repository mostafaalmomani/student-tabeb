import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ExamComponent } from 'src/app/pages/exam/exam.component';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.css']
})
export class ConfirmDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ExamComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
  }

  public decline(): void {
    this.dialogRef.close(false);
  }

  public accept(): void {
    this.dialogRef.close(true);
  }

  public dismiss(): void {
    this.dialogRef.close(false);
  }
}
