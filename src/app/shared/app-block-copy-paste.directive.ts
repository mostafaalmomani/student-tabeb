import { Directive, HostListener  } from '@angular/core';

@Directive({
  selector: '[appAppBlockCopyPaste]'
})
export class AppBlockCopyPasteDirective {

  constructor() { }
  // tslint:disable-next-line:typedef
  @HostListener('paste', ['$event']) blockPaste(e: KeyboardEvent) {
    e.preventDefault();
    alert('Error you can\'t paste');

  }

  // tslint:disable-next-line:typedef
  @HostListener('copy', ['$event']) blockCopy(e: KeyboardEvent) {
    e.preventDefault();
    alert('Error you can\'t copy');
  }

  // tslint:disable-next-line:typedef
  @HostListener('cut', ['$event']) blockCut(e: KeyboardEvent) {
    e.preventDefault();
    alert('Error you can\'t cut');
  }
}
