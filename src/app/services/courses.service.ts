import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class CoursesService extends BaseService{

  constructor(private authServices: AuthService, private http: HttpClient) {
    super();
  }

  getCoursesForStudent(): any {
    return this.http.get<{obj: any}>(`${this.apiUrl}courses/${this.authServices.getStudentId()}`);
  }

}
