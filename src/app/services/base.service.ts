import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BaseService {

 //  apiUrl = 'https://api.exams-tabeeb.org/api/v2/';
    apiUrl = 'http://localhost:80/api/v2/';
  constructor() { }
}
