import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './core/auth.guard';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { MainViewComponent } from './view/main-view/main-view.component';
import { ExamComponent } from './pages/exam/exam.component';
import { FinishPageComponent } from './pages/finish-page/finish-page.component';
import { PreventRefreshGuard } from './services/prevent-refresh.guard';


const routes: Routes = [{
  path: 'auth',
  loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)
},
{
  path: '',
  redirectTo: 'MainView/pages/dashboard',
  pathMatch: 'full'
},
{
  path: 'MainView',
  component: MainViewComponent,
  canActivate: [AuthGuard],
  resolve: {},
  children: [
    {
      path: 'pages',
      loadChildren: () => import('./pages/pages.module').then(m => m.PagesModule)
    }
  ]
},
{
  path: 'exam-view/:examid',
  component: ExamComponent,
  canDeactivate: [PreventRefreshGuard]
},
  {
    path: 'finish-page',
    component: FinishPageComponent
  },
{path: '**', component: PageNotFoundComponent}


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]

})
export class AppRoutingModule { }
